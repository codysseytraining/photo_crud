Rails.application.routes.draw do
  root 'posts#index'

  get 'rooms/show'
  get 'rooms/index', to: 'rooms#index'
  get 'search', to: 'main#search' #get :search, controller: :main
  get 'friends/index'
  get 'friends/destroy'
  resources :friend_requests
  post 'likes/up', to: "likes#up"
  post 'likes/down', to: "likes#down"
  get 'likes/show/like/:id', to: "likes#show_likes", as: 'likes_show_like'
  get 'likes/show/dislike/:id', to: "likes#show_dislikes", as: 'likes_show_dislike'

  resources :comments
  get 'pics/:id', to: "pics#show"
  delete 'pics/:id', to: "pics#del",  as: 'pics_del'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  get 'posts/profile/:id', to: "posts#profile", as: 'user_profile'
  resources :posts
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/test', to: 'application#test'

  post '/upload_profile_pic', to: 'users#upload_profile_pic'
  delete '/delete_profile_pic', to: 'users#delete_profile_pic'
  post '/update_profile', to: 'users#update_profile'
  get '/edit_profile', to: 'users#edit_profile'
  get '/edit_name', to: 'users#edit_name'
  post '/update_name', to: 'users#update_name'
  
  mount ActionCable.server => '/cable'
  
end