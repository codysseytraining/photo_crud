class FriendRequestsController < ApplicationController
  before_action :set_friend_request, except: [:index, :create, :show]

  def create
    friend = User.find(params[:format])
    @friend_request = current_user.friend_requests.new(friend: friend)
    if @friend_request.save
      redirect_back fallback_location: root_path
    else
      render json: @friend_request.errors, status: :unprocessable_entity
    end
  end

  def show
   @friend_request = FriendRequest.where(friend_id: current_user.id)
   @friend_request_outgoing = FriendRequest.where(user_id: current_user.id)
  end

# Canceling a request is equivalent to destroying a friend request record.
  def destroy
    if @friend_request.destroy
      # head :no_content
      redirect_back fallback_location: root_path
    end
  end

  def update
    if @friend_request.accept
      # head :no_content
      redirect_back fallback_location: root_path
    end
  end

  private

  def set_friend_request
    @friend_request = FriendRequest.find(params[:id])
  end

end
