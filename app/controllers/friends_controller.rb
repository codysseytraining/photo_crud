class FriendsController < ApplicationController

  def index
    @friends = User.find(params[:format]).friends
  end

  def destroy
    @friend = current_user.friends.find(params[:format])
    if current_user.friends.destroy(@friend)
      #head :no_content
      redirect_back fallback_location: root_path
    end  
  end
    
end
