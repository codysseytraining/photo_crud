class RoomsController < ApplicationController
  before_action :authenticate_user!
  def show
    @messages = Message.where(friend_id: params[:format], 
      user_id: current_user.id).or(Message.where(friend_id: current_user.id,
      user_id:params[:format] ))
    @messages.update_all(is_read: true)
  end

  def index
    @friends = current_user.friends
  end
  
end