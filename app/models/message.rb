class Message < ApplicationRecord
  belongs_to :user
  belongs_to :friend, class_name: "User"
  after_create_commit { broadcast }

 def broadcast
  BroadcastMessageJob.perform_later(self) 
 end
end
